This repository hosts vmm python entity originally developed by Xin Zhong and modified by Mikael Mieskonen and Soujanya Bhowmick.

This is an Python model to simulate the Multiply-Accumulatio Computation(MAC)operation in vector-matrix-multiplication core.



## IO feature:
    Input: 
    1. acore_clk: clock signal, integer
    2. addr: destination address in VMM hardware structure, integer
    3. d_in: data input, float number
    4. sram_wr: get 1 -> write mode, get 0 -> read mode

    Output:
    1. adc_data_ready: set 1 -> output is ready set 0 -> output is not ready yet
    2. adc_00-31 -> output port

## Method calling:
    external test bench will call entity.run(*arg), which contains the main().

    The procedure to follow is
        1) Listen to write/read mode-> Assign input image: shape 1x36 and 1x36 zero padding
        2) Assign input weights: shape 32x36
        3) Execute multiply-accumulation-operation
        4) Assign result to output ports 32x1
        5) Show adc_data_ready = 1

## Run program
    from thesdk_template:
    `source sourceme.csh`
    in vmmproject:
    `python3 vmmproject/__init__.py`
    
    


    

    


