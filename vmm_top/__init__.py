"""
=========
vmmObject
=========

vmmObject model template The System Development Kit
Used as a template for all TheSyDeKick Entities.

Current docstring documentation style is Numpy
https://numpydoc.readthedocs.io/en/latest/format.html

This text here is to remind you that documentation is important.
However, youu may find it out the even the documentation of this 
entity may be outdated and incomplete. Regardless of that, every day 
and in every way we are getting better and better :).

Initially written by Marko Kosunen, marko.kosunen@aalto.fi, 2017.

"""

import os
import sys
import math
if not (os.path.abspath('../../thesdk') in sys.path):
    sys.path.append(os.path.abspath('../../thesdk'))

from thesdk import *
from DACmodule import DAC
from ADCmodule import ADC
import numpy as np

class vmmObject(thesdk):
    @property
    def _classfile(self):
        return os.path.dirname(os.path.realpath(__file__)) + "/"+__name__

    def __init__(self,*arg): 
        self.debug_on = True       ##----------->>>> FOR DEBUG OUTPUT 
        self.debug_sram_rd_old = 0
        self.print_log(type='I', msg='Inititalizing %s' %(__name__)) 
        sys.stdout.flush()
        self.proplist = [ 'Rs' ];    # Properties that can be propagated from parent
        self.Rs =  100e6;            # Sampling frequency
        self.IOS=Bundle()            # Pointer for input data

        self.IOS.Members['acore_clk'] = IO()  # Acore clk
        self.IOS.Members['addr'] = IO()       # input addr
        self.IOS.Members['d_in'] = IO()       # input data
        self.IOS.Members['adc_data_ready'] = IO() # output is ready or not
        self.IOS.Members['sram_wr'] = IO() # write switch
        self.IOS.Members['sram_rd'] = IO() # read switch 
        
        self.IOS.Members['adc_00']= IO()   # Pointer for output data
        self.IOS.Members['adc_01']= IO()
        self.IOS.Members['adc_02']= IO()
        self.IOS.Members['adc_03']= IO()
        self.IOS.Members['adc_04']= IO()
        self.IOS.Members['adc_05']= IO()
        self.IOS.Members['adc_06']= IO()
        self.IOS.Members['adc_07']= IO()
        self.IOS.Members['adc_08']= IO()
        self.IOS.Members['adc_09']= IO()
        self.IOS.Members['adc_10']= IO()
        self.IOS.Members['adc_11']= IO()
        self.IOS.Members['adc_12']= IO()
        self.IOS.Members['adc_13']= IO()
        self.IOS.Members['adc_14']= IO()
        self.IOS.Members['adc_15']= IO()
        self.IOS.Members['adc_16']= IO()
        self.IOS.Members['adc_17']= IO()
        self.IOS.Members['adc_18']= IO()
        self.IOS.Members['adc_19']= IO()
        self.IOS.Members['adc_20']= IO()
        self.IOS.Members['adc_21']= IO()
        self.IOS.Members['adc_22']= IO()
        self.IOS.Members['adc_23']= IO()
        self.IOS.Members['adc_24']= IO()
        self.IOS.Members['adc_25']= IO()
        self.IOS.Members['adc_26']= IO()
        self.IOS.Members['adc_27']= IO()
        self.IOS.Members['adc_28']= IO()
        self.IOS.Members['adc_29']= IO()
        self.IOS.Members['adc_30']= IO()
        self.IOS.Members['adc_31']= IO()

        self.IOS.Members['d_out_1']= IO()
        self.IOS.Members['d_out_2']= IO()
        self.IOS.Members['d_out_ip_dac'] = IO()
        self.IOS.Members['data_op'] = IO()
        
        self.previous_clk = self.IOS.Members['acore_clk'].Data

        self.model='py';             # Can be set externally, but is not propagated
        self.par= False              # By default, no parallel processing
        self.queue= []               # By default, no parallel processing

        self.dac = DAC()
        self.adc = ADC()
                
        """ 
        1D structure: 34 pairs of 36 columns
        column [0] for image data, column[2:-1] for weigth_data

        Note: There are some unused blocks of memory:
            - Each column of 36 elements takes 32*2=64 addresses, but only
              the first 36 of them are used. This is due to late change in
              the design of VMM
            - Also note the unused column after input (image) data
        """
        self.VMM_data_net = [0 for i in range(34*(32*2))]

        # reset all port as 0
        for i in self.IOS.Members.keys():
            self.IOS.Members[i].Data = 0
        for i in self.adc.IOS.Members.keys():
            self.adc.IOS.Members[i].Data = 0

        if len(arg)>=1:
            parent=arg[0]
            self.copy_propval(parent,self.proplist)
            self.parent =parent


        self.init()

    def init(self):
        pass #Currently nothing to add

    def main(self):
        """Guideline. Isolate python processing to main method.
        """
        sram_wr = self.IOS.Members['sram_wr'].Data
        if (sram_wr is not None) and (sram_wr):
            self.VMM_read()
                    
        mac_list = self.mac_data()
        for i in range(len(mac_list)):
            self.VMM_write(i, mac_list[i])

    def set_ready_output(self, flag=False):
        """
        set output ready or not
        param flag: True->ready, False->not ready
        """
        if flag:
            self.IOS.Members['adc_data_ready'].Data = 1
        else:
            self.IOS.Members['adc_data_ready'].Data = 0

    def VMM_read(self):
        """
        VMM read data and address from IOS: d_in -> DAC -> self.VMM_data_net
        addressing range: data: 0->36, weights: 72->34*36-1
        """
        address = None
        address = int(self.IOS.Members['addr'].Data.binstr,2)

        
        data_in = self.IOS.Members['d_in'].Data

        
        self.dac.IOS.Members['d_in'].Data = data_in
        self.dac.IOS.Members['d_type'].Data = 1 if self.addressto_weights_image(address) \
                                            else 0

        self.dac.run()
        data_out = self.dac.IOS.Members['d_out'].Data

        """ as numeric type"""
            # self.VMM_data_net[1][address] = data

        """ as binary type"""
        if (address is not None) and (data_out is not None):
            self.VMM_data_net[address] = data_out


    def addressto_weights_image(self, address):
        """
        determine the address of feeding to image or weigths
        address: from 0 to 34*36-1
        return False: image data, True: weights 
        """
        if (0<=address<36):
            # data
            return False
        else:
            # weights
            return True

    def VMM_write(self, address, data=None):
        """VMM write data to the indexst output port
        param data: input data
        param adress:  index of the output port
        """
        if data is not None:
            self.IOS.Members['adc_{:02d}'.format(address)].Data = data

        return self.IOS.Members['adc_{:02d}'.format(address)].Data

    def mac_data(self):
        
    
        """
        convolution process
        param: data_list shape 1x36
        param: weight_list shape 32x36  with weights and zero padding
        return: mac_list 32x1
        """
        
        mac_list = [0 for i in range(32)]  #initializes a list called mac_list with 32 zeros. This list will eventually store the results of the convolution operation.
        
        image_list = self.VMM_data_net[0:36]  # extracts the first 36 elements from the VMM_data_net attribute of the class instance. This slice represents the input image data used in the convolution.

        weight_list = []
        for i in range(36*32):
            block_start = (64*2) + 64*i
            # Don't include unused memory addresses of `VMM_data_net` in `the weight_list`
            weight_list += self.VMM_data_net[block_start : block_start + 36]

        num_col = int(len(weight_list)/36)


        gain_factor = 36  ## not used 
        #magic_number =  4 # no magic number 
        bias = 0.5 #not used
        
        
        feed_list = []
        for i in range(num_col): # each column by 32
            data_ = 0
            for h in range(0+36*i, 36+36*i): # each row  by 36
                data_ += image_list[h-36*i]*weight_list[h]
  
  
            data = data_ / 36
            feed_list.append(data)          #debug
            
            
            if self.debug_on:
                    sram_rd = self.IOS.Members['sram_rd'].Data
                    if (sram_rd == 1) and (self.debug_sram_rd_old == 0):
                        print (feed_list)
                        print (" ")
                        sys.stdout.flush() # This will flush the output buffer to ensure it prints immediately


        for i in range(len(feed_list)):
            self.adc.IOS.Members['din_{:02d}'.format(i)].Data = feed_list[i]
        self.adc.run()

        for i in range(len(mac_list)):
            mac_list[i] = self.adc.IOS.Members['adc_{:02d}'.format(i)].Data
            

################################################################################


################################################################################

################################# THIS IS FOR DEBUG OUTPUT CODE BELOW ###############





        if self.debug_on:
            sram_rd = self.IOS.Members['sram_rd'].Data
            if (sram_rd == 1) and (self.debug_sram_rd_old == 0):
                print ("Hello, This is testing" )
                print ("")
                
                sys.stdout.flush()

            
            
            
        if self.debug_on:
            sram_rd = self.IOS.Members['sram_rd'].Data
            if (sram_rd == 1) and (self.debug_sram_rd_old == 0):
                print(f"Input: {image_list}")
                weight_list_debug = []
                print("Weights:")
                for i in range(0, len(weight_list), 36):
                    weight_list_debug.append(weight_list[i:i+36])
                weight_list_debug = np.array(weight_list_debug)
                weight_list_debug = weight_list_debug.transpose()
                weight_list_debug = weight_list_debug.tolist()
                for i in weight_list_debug:
                    print(', '.join('{:.3f}'.format(f) for f in i))
                print(f"Output (float): {feed_list}")
                print(f"Output (binary): {mac_list}")
                sys.stdout.flush()
            self.debug_sram_rd_old = sram_rd
            
        """
            
        # Debug output to observe all values processed
        if self.debug_on:
            sram_rd = self.IOS.Members['sram_rd'].Data
            if (sram_rd == 1) and (self.debug_sram_rd_old == 0):
        
                print("Debug Information:")
                print("Image Data:")
                print(image_list)
                print("Weight List:")
                print(weight_list)
                print("Feed List (results of convolution per column):")
                print(feed_list)
                print("MAC List (final ADC outputs):")
                print(mac_list)
                print("---------------------------------------------------")
                
                sys.stdout.flush()
            self.debug_sram_rd_old = sram_rd
                
        """               
        
###################### ABOVE IS DEBUG CODE #########################################
  
       

        return mac_list
        
    def rescale_sum(self, sum_):  ###################### NOT USED 
        gain_factor = 36 
        #magic_number = 4
        bias = 0.5

        sum_ /= gain_factor
        sum_ += bias
        sum_ *= 2**6
        print("sum_ before ceil: ", sum_)
        sum_ = math.ceil(sum_)
        sum_ = float("{:.6f}".format(sum_))
        # print("sum_ after ceil: ", sum_)
        return sum_
        
        

    def rescale_data(self, image_, weight_): ############# NOT USED 
        bias = 0.5

        """here should be done in C model before feeding VMM"""
        image_ = (image_ - bias)/(-1*bias)
        weight_ = weight_/(-1*bias)

        image_ = image_*(-1*bias) + bias
        weight_ = weight_ * (-1*bias)

        return image_, weight_
        
        
        

    def quantize_linear_output(array, bits):
        """
        quantize data by resolution
        param array: the input array
        param bits: the resolution scale, represented by bits in binary
        return result: quantized data
        """
        max_v = np.amax(array)
        min_v = np.amin(array)
    
        resolution = (max_v-min_v)/(2**(bits))
        result = resolution*np.round(array/resolution)
        return result

    def reverse(self, message):
        str = ""
        for i in message:
            str = i + str
        return str


    def run(self,*arg):
        """Guideline: Define model depencies of executions in `run` method.
        """
        if len(arg)>0:
            self.par=True      #flag for parallel processing
            self.queue=arg[0]  #multiprocessing.queue as the first argument
        if self.model=='py':
            self.main()
            
if __name__=="__main__":
    import matplotlib.pyplot as plt
    from controller import controller as vmm_top_controller
    import pdb
    import math
    length=1024
    rs=100e6
    indata=np.cos(2*math.pi/length*np.arange(length)).reshape(-1,1) # range of vmm input filter matrix

    models=[ 'py']
    duts=[]
    for model in models:
        d=vmmObject()
        duts.append(d) 
        d.model=model
        d.Rs=rs
        d.init()
        d.run()
    
    input()


